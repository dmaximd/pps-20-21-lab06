package u06lab.code

object TicTacToe extends App {
  val winningPositions = List((0, 1), (1, 0), (1, 1), (1, 2), (2, 1))

  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Int, y: Int): Option[Player] = board.filter(m => m.x == x && m.y == y).map(m => m.player).headOption

  def placeAnyMark(board: Board, player: Player): Seq[Board] = for {
    x <- 0 to 2
    y <- 0 to 2
    mark = Mark(x, y, player)
    if find(board, x, y).isEmpty
  } yield mark :: board

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(List.empty))
    case _ => for {
      game <- computeAnyGame(player.other, moves - 1)
      nextBoard <- if (canGameContinue(game.head)) placeAnyMark(game.head, player) else List(Nil)
    } yield if (canGameContinue(game.head)) nextBoard :: game else game
  }

  private def canGameContinue(board: Board): Boolean = {
    var found = false
    for (x <- 0 to 2; if !found) found = areOfSamePlayer(board, List((x, 0), (x, 1), (x, 2)))
    for (y <- 0 to 2; if !found) found = areOfSamePlayer(board, List((0, y), (1, y), (2, y)))
    if (!found) found = areOfSamePlayer(board, List((0, 0), (1, 1), (2, 2)))
    if (!found) found = areOfSamePlayer(board, List((0, 2), (1, 1), (2, 0)))
    !found
  }

  private def areOfSamePlayer(b: Board, l: List[(Int, Int)]): Boolean = {
    val opt = l.map(pos => find(b, pos._1, pos._2)).distinct
    opt.length == 1 && opt.head.isDefined
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  println()
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  //Test additional functions
  val boardDiag = List(Mark(0, 0, X), Mark(1, 1, X), Mark(2, 2, X))
  val listDiag = List((0,0), (1,1), (2,2))
  val boardCol = List(Mark(0, 0, X), Mark(1, 0, X), Mark(2, 0, X))
  val listCol = List((0, 0), (1, 0), (2, 0))
  val boardNotFullCol = List(Mark(0, 0, X), Mark(1, 0, X))
  val boardNotAcceptedCol = List(Mark(0, 0, X), Mark(1, 0, X), Mark(2, 0, O))
  println(areOfSamePlayer(boardDiag, listDiag))           //true
  println(areOfSamePlayer(boardCol, listCol))             //true
  println(areOfSamePlayer(boardCol, listDiag))            //false
  println(areOfSamePlayer(boardNotFullCol, listDiag))     //false
  println(areOfSamePlayer(boardNotAcceptedCol, listCol))  //false

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  println()
  var total = 0
  var countInFiveMoves = 0
  computeAnyGame(O, 6) foreach {g => printBoards(g); total = total + 1; if (g.length == 6) countInFiveMoves = countInFiveMoves + 1; println()}
  println("Won at 5th iteration: " + countInFiveMoves) //(8 * (3 * 2)) * 6 * 5 = 1440
  println("Total: " + total)
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}
